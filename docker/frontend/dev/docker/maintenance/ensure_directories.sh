#!/usr/bin/env sh

# --parents: parents_create, exist_ok
mkdir --parents "node_modules" &&
  chmod 777 -R ./node_modules &&
  ls -lh \
  ;
