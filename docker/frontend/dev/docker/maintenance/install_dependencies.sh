#!/usr/bin/env sh

npm config set package-lock true &&
  echo "[install_dependency]-[BEGIN]" &&
  npm install &&
  echo "[install_dependency]-[END]" &&
  echo "[install_fixes]-[BEGIN]" &&
  npm audit fix &&
  echo "[install_fixes]-[END]" \
  ;
