.PHONY: d-i-frontend-local-dev-i-build
d-i-frontend-local-dev-i-build:
	@# Don't silent. For showing used data. ? Clean "data-usage" ?
	DOCKER_BUILDKIT=1   docker build \
		--file docker/frontend/dev/docker/Dockerfile \
		--tag local/grandcore_vue_dev \
 		--pull \
 		--build-arg USER_ID=$(shell id --user ${USER}) \
 		--build-arg GROUP_ID=$(shell id --group ${USER}) \
 		.

.PHONY: d-i-frontend-local-dev-i-ensure-directories
d-i-frontend-local-dev-i-ensure-directories:
	@# MUST be attached, for make actions after this. Better if "auto-clear" containers. For reduce used space and for more "predictable" results.
	@docker-compose \
		--file ./docker-compose.dev.yml --project-name grandcore \
		rm --stop --force frontend-ensure
	@docker-compose \
		--file ./docker-compose.dev.yml --project-name grandcore \
		up --no-build frontend-ensure
	@docker-compose \
		--file ./docker-compose.dev.yml --project-name grandcore \
		rm --stop --force frontend-ensure

.PHONY: d-i-frontend-local-dev-i-install-attached
d-i-frontend-local-dev-i-install-attached:
	@# MUST be attached, for make actions after this. Better if "auto-clear" containers. For reduce used space and for more "predictable" results.
	@make d-i-frontend-local-dev-i-ensure-directories
	@docker-compose \
		--file ./docker-compose.dev.yml --project-name grandcore \
		rm --stop --force frontend-install
	@docker-compose \
		--file ./docker-compose.dev.yml --project-name grandcore \
		up --no-build frontend-install
	@docker-compose \
		--file ./docker-compose.dev.yml --project-name grandcore \
		rm --stop --force frontend-install

.PHONY: d-i-frontend-local-dev-i-remove-now
d-i-frontend-local-dev-i-remove-now:
	@docker-compose \
		--file ./docker-compose.dev.yml --project-name grandcore \
		kill frontend-dev
	@docker-compose \
		--file ./docker-compose.dev.yml --project-name grandcore \
		rm --stop --force frontend-dev

.PHONY: d-i-frontend-local-dev-i-up-detach
d-i-frontend-local-dev-i-up-detach:
	@docker-compose \
		--file ./docker-compose.dev.yml --project-name grandcore \
		up --no-build --detach frontend-dev

.PHONY: d-i-frontend-local-dev-i-logs-follow
d-i-frontend-local-dev-i-logs-follow:
	@docker-compose \
		--file ./docker-compose.dev.yml --project-name grandcore \
		logs --follow frontend-dev

.PHONY: d-i-frontend-local-dev-i-restart-with-build
d-i-frontend-local-dev-i-restart-with-build:
	# Stop only after having good "build" and "install"
	make d-i-frontend-local-dev-i-build && \
	make d-i-frontend-local-dev-i-install-attached && \
	make d-i-frontend-local-dev-i-remove-now && \
	make d-i-frontend-local-dev-i-up-detach && \
	make d-i-frontend-local-dev-i-logs-follow

.PHONY: d-i-frontend-local-dev-i-frontend-manual-update
d-i-frontend-local-dev-i-frontend-manual-update:
	@# Example: FLAGS="npm install --save-exact vuex" make d-i-frontend-local-dev-i-frontend-manual-update
	make d-i-frontend-local-dev-i-build
	docker-compose \
		--file ./docker-compose.dev.yml --project-name grandcore \
		run --rm frontend-manual-update $(FLAGS)

.PHONY: d-i-frontend-local-dev-i-frontend-manual-update-i-npm-check
d-i-frontend-local-dev-i-frontend-manual-update-i-npm-check:
	FLAGS="npm-check --update --save-exact" make d-i-frontend-local-dev-i-frontend-manual-update

# Run this for dev
.PHONY: d-dev-run
d-dev-run:
	make d-i-frontend-local-dev-i-restart-with-build


# Run this for kill dev
.PHONY: d-dev-kill
d-dev-kill:
	make d-i-frontend-local-dev-i-remove-now


# Run this for kill dev
.PHONY: d-dev-update
d-dev-update:
	make d-i-frontend-local-dev-i-frontend-manual-update-i-npm-check
