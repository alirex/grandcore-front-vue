Чат проекта - https://t.me/joinchat/OQ6DaxfW8D5E-_qYCsxhZw

# grandcore-platform-front

> Nuxt.js project

##Docker-based dev run

``` bash
# Run (or re-rerun) dev instance at 43000 port, 
#   in container,
#   with installing requirements,
#   with binding node-modules in project folder for IDE
$ make d-dev-run

# Kill dev instance
$ make d-dev-kill

# Run "npm-check" for updating dependencies in interactive mode
$ make d-dev-update

# More details in Makefile
```

## In-system Build Setup

``` bash
# install dependencies
$ npm install # Or yarn install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, checkout the [Nuxt.js docs](https://github.com/nuxt/nuxt.js).

